import React from 'react';
import Table from "./Table";
import './containers/App.css';


const FullTable = (props) => {
    return (
        <div className="Table">
            {props.squares.map((square) => <Table click={() => props.click(square.id)} key={square.id} clicked={square.clicked} hasItem={square.hasItem}/>, )}
        </div>
    )
}

export default FullTable;