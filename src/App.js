import React, {Component} from 'react';
import './containers/App.css';
import FullTable from './FullTable';



class App extends Component {
    state = {
        data: [ ],
        something: '',
        counter: 0
        }

    componentDidMount = () => {
        const data = this.Squares()
        let counter = 0;

        this.setState({data, counter})

    }

    Squares = () => {
      const sqArray = [];
        for (let i = 0; i < 36; i++){
            let square = { hasItem: false,
                            clicked: false,
                            id: i + 1}
            sqArray.push(square);
      }
      const index = Math.floor(Math.random() * sqArray.length);
        sqArray[index].hasItem = true;
      return sqArray
    };



    changeColor = id => {
        console.log(id);
        const index = this.state.data.findIndex(d => d.id === id);
        let counter = this.state.counter;

        let data = [...this.state.data];
        if(!data[index].clicked) counter++
        console.log(counter)
        data[index].clicked = true;
        this.setState({data, counter});

    };




    render () {
        return (
            <div>
                <FullTable id="container" click={this.changeColor} squares={this.state.data} />
                <p className="tries">Tries: {this.state.counter}</p>
                <button className='reset' onClick={this.componentDidMount}>Reset</button>
            </div>
        )
    }
}

export default App;
