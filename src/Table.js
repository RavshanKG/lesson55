import React from 'react';


const clicked = {
    backgroundColor: '#48a026',
    width: '40px',
    height: '40px',
    display: 'inline-block',
    border: '2px solid white'
};

const notClicked = {
    backgroundColor: '#2655a0',
    width: '40px',
    height: '40px',
    display: 'inline-block',
    border: '2px solid white'
};

const Table = (props) => {
    return (
            <div onClick={props.click} className="cell" style={props.clicked ? clicked : notClicked}>{props.clicked && props.hasItem ? 'o' : null}</div>
    );
}

export default Table;


